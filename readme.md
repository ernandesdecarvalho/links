## Links

Repositório de teste e aprendizado de funcionalidades do controle de versão usando GIT.

## ANDROID
[Android Developers](http://developer.android.com/index.html)

## PHP
[php.net](http://php.net)

## STACKOVERFLOW BRASIL
[pt.stackoverflow.com/](http://pt.stackoverflow.com/)

## SQL SERVER
[Instruções DML](http://msdn.microsoft.com/pt-br/library/ff848766.aspx)
